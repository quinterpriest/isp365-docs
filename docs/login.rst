.. _login:

Login
=====

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/linzs0Gb_Tg" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>


En español ingresar o entrar es el proceso mediante el cual se controla el acceso individual a un sistema informático.