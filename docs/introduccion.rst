.. _introduccion:

Introducción
============

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/Oh3vJKEZg2c" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

En el video anterior se muestra Business 365 Es un sistema de gestión empresarial, ahora tendrá siempre a disposición la información detallada y actualizada de los diferentes módulos de Superredes.

Es importante para nosotros poner a su disposicion la documentación de la plataforma B365, en las siguientes pantallas prepárese para adquirir el conocimiento necesario para gestionar su empresa con B365, aprenderas desde iniciar sesión en la plataforma como recuperar una cuenta, búsquedas de registros, crear registros nuevos.

La pantalla principal del sitio web esta dividia en tres partes, un menú lateral izquierdo en el cual muestra el nombre de la persona logueada, el nombre de la empresa y unidad organizacional, cada uno de los diferentes módulos que tiene habilitados la persona, cada módulo como mínimo tendrá una sección de configuración para configurar dicho modulo y una sección de reportes, también cuenta con una barra superior que se compone de un botón para mostrar la pantalla completa, un botón para ir al pos(no aplica para la empresa), el nombre de la persona que se encuentra logueada, opciones para cambiar el perfil, cambiar la sucursal, salir y la tercera parte es el contenido que siempre nos mostrará en una pestaña el nombre del módulo o la sección en la cual estamos trabajando a su lado una imagen de un video que nos lleva a la documentación con el video explicativo de la sección actual unos campos para filtrar un listado en la parte inferior, un botón de nuevo para crear un nuevo registro, un botón para limpiar dichos filtros, otro para buscar por los filtros seleccionados y el listado con diferentes opciones por registro el cual se puede ordenar por sus columnas en orden ascendente o descendente.

.. rubric:: Notas

.. [#f1]

    *Terminos relacionados :term:`Login`, :term:`Contraseña`, :term:`Autocompletar`, :term:`Lista Desplegable`, :term:`Filtros de búsqueda`, :term:`CSV`. 
