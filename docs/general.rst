.. _general:

Módulo General
===============
 Desde este módulo puedes gestionar Personas, Días no hábiles, Tipos de Contrato, Estratos Sociales, Proyectos, Paises, Pasarelas de Pago, Departamentos, Barrios, Cargos, Ciudades, Comunas, Consecutivos, Géneros, Monedas, Configuración Empresa, Empleados, Unidades Organizacionales, Sucursales, Vehículos, Listaas de Valores y Palntillas Documento Anexo.

Sección Personas
----------------

    Descripción
    -----------

Esta sección permite: Crear, Listar, Buscar ,Editar o Eliminar una persona. Debe agregarlas primero aquí, empleados, clientes.

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/vaL36n1BGUA" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`> General> Personas`

.. image:: images/general/general1.png
  :width: 550
  :align: center
  :alt: Pos pantalla login

Sección Días no hábiles
-----------------------   

    Descripción
    -----------

Esta sección permite: Crear, Listar, Buscar ,Editar o Eliminar días no hábiles. Se refiere a los días festivos que se deben configurar, para que cuando ese día caiga el sistema automáticamente coloque la fecha de pago el día hábil siguiente.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/aJy1Qz8Yr7c" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Días no hábiles`   

Sección Tipos de Contrato
-------------------------   

    Descripción
    -----------

Esta sección permite:  crear,listar,editar,eliminar los diferentes tipos de contrato que tiene Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/DWXSVjY5Tno" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Tipos de Contrato`   

Sección Estratos Sociales
-------------------------   

    Descripción
    -----------

Esta sección permite:  crear,listar,editar,eliminar los diferentes estratos sociales en Colombia. Los estratos sociales en los que se pueden clasificar las viviendas son 6, denominados así: 1. Bajo-bajo 2. Bajo 3. Medio-bajo 4. Medio 5. Medio-alto 6. Alto.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/sbZbtgUGIn0" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Estratos Sociales`

Sección Proyectos
-----------------

    Descripción
    -----------

Esta sección permite:   Crear,Buscar,Editar,Borrar y Listar proyectos en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/V-14ECNwTLM" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Proyectos`

Sección Paises
--------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Paises en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/vOrYs2RbS94" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Paises`

Sección Pasarelas de Pago
-------------------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Pasarelas de Pago en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/0aUFr9PJEAA" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Pasarelas Pago`

Sección Departamentos
---------------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Departamentos de Colombia, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/4WHVc5el5gs" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Departamentos`

Sección Barrios
---------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Barrios de Colombia, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/qNFAL2tIJvk" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Barrios`

Sección Cargos
--------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Cargos, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/zOkAdtv4X_o" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Cargos`

Sección Ciudades
----------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Ciudades de Colombia, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/9lKlodvxqBo" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Ciudades`

Sección Comunas
---------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Comunas de Colombia, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/WFRp0ZNIGzc" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Comunas`

Sección Consecutivos
--------------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Consecutivos para los diferentes documentos, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/6Bn64358Poc" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Consecutivos`

Sección Géneros
---------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Generos, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/hy5EcgtwmWs" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Géneros`

Sección Monedas
---------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Monedas, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/w4VHJReX4a0" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`General> Configuración> Monedas`

Sección Configuración Empresa
-----------------------------

    Descripción
    -----------

Esta sección permite:   configurar una empresa para la facturación en este caso Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/ctDhnh-jYdE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`> General> Configuración Empresa`

Sección Empleados
-----------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Empleados, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/mhA5dl1fUxk" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`> General> Empleados`

Sección Unidades Organizacionales
---------------------------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Unidades Organizacionales, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/CT-nc8sH5Yc" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`> General> Unidades Organizacionales`

Sección Sucursales
------------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Sucursales, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/C2eXiXH8W5A" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`> General> Sucursales`

Sección Vehículos
-----------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Vehiculos, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/D1FDpGzpGH4" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`> General> Vehículos`

Sección Lista Valores
---------------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Listas de Valores para usar en los diferentes documentos, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/SWFOl91rZAc" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`> General> Lista Valores`

Sección Plantilla Documento Anexo
---------------------------------

    Descripción
    -----------

Esta sección permite:   Crear, Editar, Borrar, Buscar, Listar Plantilla Documento Anexo para usar en los diferentes documentos, en la empresa Superredes.
   
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/0SIzqY-dXQE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    	
Ruta::file:`> General> Plantilla Documento Anexo`