.. index:: config.inc.php

.. _facturacion:

Facturación
===========

Es el documento físico que constata de la operación de compra y venta de un bien o 
Servicio, la cual cuenta con toda la información de la transacción con datos como fecha y lugar, datos del vendedor, precios y IVA.
Por medio de la facturación es posible las reclamaciones, garantías y verificar si cliente efectivamente nos compró un producto. 

En Colombia solo existen 4 tipos de Métodos para la facturación avalados por la DIAN
Dirección de Impuestos y  Aduanas Nacionales

Nosotros contamos con uno de los 4 métodos avalados  que es la facturación POS
O más bien con dos de los 4 métodos por que la facturación por computador también 
es un método de facturación avalado más bien somos una combinación en poco tiempo 

Nuestro sistema también dispondra de la facturación electrónica.

Regresando con la facturación POS es una modalidad de bastante de uso a nivel nacional 
Empleado mayor por almacenes con alto volumen de transacciones ya que este sistema mejora el rendimiento del proceso en gran cantidad, también es muy utilizado por establecimientos comerciales medianos como las pequeñas y medianas PYMES.

 La facturación POS consiste en considerar factura el tiquete generado por la máquina registradora en el momento en que se hace el pago de un bien o servicio, partiendo de la base que dicho tiquete ha sido generado por medio de un software especializado aceptado por la Dirección de Impuestos y Aduanas Nacionales (DIAN).

Este tiquete debe cumplir con los siguientes requisitos:

    Nombre o razón social y número de identificación tributaria (NIT) del vendedor del bien o prestador del servicio.

    Número de la transacción, esto debido a que todas las facturas deben seguir un orden consecutivo de numeración, en el caso de las máquinas registradoras, puede contar con nominaciones alfanuméricas, sin que exceda cuatro caracteres.

    Fecha de la operación.

    Descripción del servicio o bien adquirido. 

    Valor total de la transacción.

Configuración de Resolución de Facturación
------------------------------------------
*Obligación
La resolución de facturación la expidió la Dian en la resolución 00055 de julio 21 de 2016, la cual reglamentó la obligación de solicitar autorización para facturar, por lo tanto esta es de obligatorio cumplimiento.

*Prefijo
Cuando el contribuyente tiene varios establecimientos de comercio, requiere identificar o separar las ventas según sus necesidades particulares, puede recurrir a los prefijos como mecanismo auxiliar para identificar las facturas y los ingresos.

*Sanciones 
a vimos que uno de los requisitos de la factura es contener la resolución de facturación, de modo que si no se tiene, o se gestiona pero no se imprime en la factura, estamos ante un hecho irregular relacionado con la facturación, y se impone la respectiva sanción por facturación.

Paso a Paso

**1) Boton :** :guilabel:`Nuevo`.

**2) Sucursal:**click en el campo se abre una lista desplegable con las surcursables diponibles elegi una (*Obligatorio).

**3) Nombre:** escriba un nombre para su consecutivo(*Obligatorio).

**4) Sufijo:** este no es (*Obligatorio) y en la mayoria los casos este numero impuesto a gusto por las organizacion el numero digitado aqui va impreso en la factura el orden este es antes del # de factura.

**5) Numero Inicial:** para completar este dato debemos de tener el documento Solicitud sobre numeracion de facturacion de la Dian en el campo 27. Desde el numero es el dato que debemols digitar aqui (*Obligatorio).

.. image:: images/dian_factura27.jpg

Tarifas por sucursales
----------------------
Con opción brindamos la posibilidad de gestionar diferencias en entre los precios para nuestras sucursales de negocio por su localidad o posición geográfica.

Paso a Paso

Crear Tarifa por Sucursal
++++++++++++++++++++++++++

**1) Nombre:** Digitar el nombre que va tener la taruifa por sucursal.

**2) activo:** (casilla de verificacion o checkbox) Marca la casilla para activar la tarifa por sucursal.

Productos
----------

**3) Producto:** Digita el nombre del producto o da click en la lupa para iniciar una busqueda avanzada por filtros.

**4) Precio Venta:** Es precio que va tener el producto al cliente final.

**1) Impuesto:** :guilabel:`Guardar`.

.. Hint:: https://erpv3.source-solutions.co/facturacion/tarifa-por-sucursal/list.

Clientes
--------

Esta sección se encuentra dentro del módulo Facturación → Clientes.

.. image:: images/facturacion/Clientes1.png

Está primera pantalla nos muestra un listado de los clientes que se encuentran creados en el Business acá podemos filtrar por Nombres, No. Documento y Activo (estado del cliente) a continuación se muestra cada uno de los escenarios de búsqueda:

**1. Nombres**
++++++++++++++

Ejemplo filtraremos por los que contengan “Maria” en sus Nombres, para realizarlo en el campo de Texto al lado de Nombres digitamos “Maria” y presionamos el botón verde que dice buscar con una Lupa y el siguiente es el resultado:"

.. image:: images/facturacion/Clientes2.png

como observamos en la imagen anterior el filtro nos indica que existen 1458 registros que contienen Maria como su primer o segundo Nombre, en la tabla resultado podemos dar clic sobre cada uno de los títulos Azules (Persona, Término de Pago, Tipo Régimen, Observaciones o Activo.

.. image:: images/facturacion/Clientes3.png

En la imagen anterior, se dio clic sobre el Titulo Persona y podemos observar que sale un Triángulo que indica que están ordenados de forma ascendente por dicha columna.

**2. Nombre y No. Documento**
+++++++++++++++++++++++++++++

Filtraremos por los clientes que contengan “Maria” en sus Nombres y que contengan “108801” en su No. Documento, en los campos de Texto Nombres digitamos “Maria”, en el campo de Texto No. Documento digitamos “108801” y presionamos el botón verde que dice buscar con una imagen de Lupa y el siguiente es el resultado:

.. image:: images/facturacion/Clientes4.png

en la imagen anterior 2 registros que cumplen con dos criterios de búsqueda.

**3. Nombre, No. Documento y Activo**
+++++++++++++++++++++++++++++++++++++

Filtraremos por los clientes que contengan “Maria” en sus Nombres,  que contengan “108801” en su No. Documento y “Si” en su estado Activo, en los campos de Texto Nombres digitamos “Maria”, en el campo de Texto No. Documento digitamos “108801”, en el campo Activo seleccionamos “Si” y presionamos el botón verde que dice buscar con una imagen de Lupa y el siguiente es el resultado: 

.. image:: images/facturacion/Clientes5.png

la imagen anterior muestra el resultado del Filtro.

**4. Botón Limpiar Filtros con imagen borrador**
++++++++++++++++++++++++++++++++++++++++++++++++

Deja en blanco los campos de texto para ingresar otra información al presionarlo este será el resultado:

.. image:: images/facturacion/Clientes7.png

**5. Botones en la tabla Eliminar, Duplicar, Editar**
+++++++++++++++++++++++++++++++++++++++++++++++++++++

.. image:: images/facturacion/Clientes8.png

**5a. Botón Eliminar**
++++++++++++++++++++++

En la tabla al lado derecho el icono en forma de circulo con una barra inclinada dentro de el, al ubicar el cursor sobre el nos mostrará el nombre asignado al botón como en la siguiente imagen.

.. image:: images/facturacion/Clientes9.png

Al dar clic en dicho botón nos mostrará una ventana donde nos preguntará si estamos seguros de eliminar dicho cliente, como en la siguiente imagen:

.. image:: images/facturacion/Clientes10.png

podemos decidir Eliminar (Nota: El cliente no puede tener contratos asociados) o Cancelar, vamos a ver los dos escenarios, Eliminar: en este escenario el cliente tiene un contrato por lo tanto nos muestra el mensaje y no permite eliminarlo. Y si le damos cancelar no se ejecuta ninguna acción.

.. image:: images/facturacion/Clientes11.png

.. image:: images/facturacion/Clientes9.png

**5b. Botón Duplicar**
++++++++++++++++++++++

En la tabla al lado derecho el icono en forma de cuadro negro , al ubicar el cursor sobre el nos mostrará el nombre asignado al botón como en la siguiente imagen. 

.. image:: images/facturacion/Clientes12.png

Al dar clic en dicho botón nos saldrán datos como si fuésemos a editar un cliente permitiendo tener datos ya llenos como en la siguiente imagen:

.. image:: images/facturacion/Clientes13.png

para la prueba lo dejare con estos datos y le damos clic en el botón que dice Guardar, nos muestra un mensaje que el registro fue modificado correctamente.

.. image:: images/facturacion/Clientes14.png

Y observamos que nos duplico el registro inicial, como la imagen siguiente:

.. image:: images/facturacion/Clientes15.png

Ahora procederemos a borrar el registro duplicado, realizamos el paso 5a nuevamente y daremos clic en Eliminar.

.. image:: images/facturacion/Clientes16.png

Y observamos que nos muestra el mensaje que confirma que el borrado.

.. image:: images/facturacion/Clientes17.png

Y la pantalla se actualiza y nos muestra solamente un solo registro:

.. image:: images/facturacion/Clientes18.png

**5c. Botón Editar**
++++++++++++++++++++

En la tabla al lado derecho el icono en forma de lápiz negro , al ubicar el cursor sobre el nos mostrará el nombre asignado al botón como en la siguiente imagen. 

.. image:: images/facturacion/Clientes19.png

Al dar clic en dicho botón nos saldrán datos para editar el cliente del registro seleccionado,  como en la siguiente imagen:

.. image:: images/facturacion/Clientes20.png

En esta pantalla podemos editar los datos que necesitemos,  para el ejemplo voy a editar el nombre por Maria Rubiela, voy a cambiar la dirección CR 5 32 63 y el celular 3108345797, como se muestra en la siguiente imagen, NOTA: para la dirección debemos dar clic sobre el siguiente botón 	y contamos con una ventana

.. image:: images/facturacion/Clientes21.png

con dos pestañas Nomenclaturas (ejemplo Carrera, Calle, Lote, guión(-) numeral(#)  etc ) y Teclado (con los números 0-9, letras A-Z y BIS) como en las siguientes imágenes:

.. image:: images/facturacion/Clientes22.png

.. image:: images/facturacion/Clientes23.png

Se debe presionar la nomenclatura deseada y/o el carácter del teclado necesarios,  los cuales se mostrarán en el campo de texto Dirección, Dian y Dir como en la siguiente imagen:

.. image:: images/facturacion/Clientes24.png

para cerrar esta pantalla, damos clic por fuera de la pantalla  y damos clic en el botón Guardar. Nos muestra un mensaje "El registro fue modificado correctamente" como en la siguiente imagen:

.. image:: images/facturacion/Clientes25.png

y consultamos el registro modificado : 

.. image:: images/facturacion/Clientes26.png

**5d. Clientes→ Botón Nuevo**
+++++++++++++++++++++++++++++

.. image:: images/facturacion/Clientes27.png

 → Nos permite crear un nuevo cliente en el sistema. En la pantalla de Clientes damos clic en el Botón  y nos muestra la siguiente ventana:

.. image:: images/facturacion/Clientes28.png

los datos anteriores se puede observar que hay algunos con un asterisco (*) en color rojo, esto indica que el valor es obligatorio, 
Nota: “el campo Término de Pago siempre será 10 días se va a colocar obligatorio y que este este valor único para seleccionar.”

.. image:: images/facturacion/Clientes29.png
   :alt: alternate text
   :align: left

, y en No. Documento podemos notar que hay dos cuadros de texto el de la derecha es para colocar el dígito de verificación cuando el tipo de documento es NIT.

.. image:: images/facturacion/Clientes30.png
   :height: 50 px
   :width: 200 px
   :scale: 50 %
   :alt: alternate text
   :align: left

y el botón  Adicionar ubicación

es para agregar mas direcciones del cliente. Los demás campos son opcionales, ingresaré unos valores y daré clic al botón Guardar.

.. image:: images/facturacion/Clientes31.png

Y ahora consultamos el registro creado:

.. image:: images/facturacion/Clientes32.png

**5e. Clientes→ Botón Importar**
++++++++++++++++++++++++++++++++

.. image:: images/facturacion/Clientes33.png
   :height: 50 px
   :width: 200 px
   :scale: 50 %
   :alt: alternate text
   :align: left
 
Nos permite importar un archivo csv o excel para crear varios clientes al tiempo pasos:

**1.** clic al botón Importar, al dar clic nos muestra una ventana en la cual debemos descargar una plantilla , llenarla como se muestra en la imagen y luego dar clic al botón Seleccionar archivo en la parte inferior.
**1.1** Descargar Plantilla→ al dar clic a este botón el sistema nos descarga la siguiente plantilla: b365_importar_clientes.csv al abrir este archivo observamos su contenido(el cual contiene un ejemplo de como se debe llenar el campo tipo de identificación, Ciudad Ubicación y Término de pago
Nota: este último será 10):

.. image:: images/facturacion/Clientes34.png

**1.2 llenado del archivo** → voy a colocar 3 clientes en el archivo:

.. image:: images/facturacion/Clientes35.png

**1.3 subida del archivo** → presionamos el botón seleccionar archivo, y nos permite seleccionar el archivo, debemos ir a la ubicación donde lo tengamos:

.. image:: images/facturacion/Clientes36.png

**1.4 damos clic en Abrir**

.. image:: images/facturacion/Clientes37.png

el sistema nos dice que encontró 3 filas para importar presionamos siguiente:

.. image:: images/facturacion/Clientes38.png

nos dice que el proceso puede tardar unos minutos dependiendo de la cantidad de filas que contenga el archivo, presionamos importar:

.. image:: images/facturacion/Clientes39.png

nos muestra un mensaje de filas procesadas, damos clic en cerrar y verificamos los clientes:

.. image:: images/facturacion/Clientes40.png

.. image:: images/facturacion/Clientes41.png

.. image:: images/facturacion/Clientes42.png



Creación de Facturas
---------------------

**1) Boton :** :guilabel:`Nuevo`.

**2) Fecha:** click en el icono de calendario y elecciona la fecha que desee para crear su factura. 

**3) Sucursal:** click en el campo se despleiga una lista de las surcursales disponibles.

**4) Cliente:** en este campo puedes digitar el nombre o numero de cedula de un cliente o buscar con la lupa con filtros de busqueda avanzada.

**5) Vendedor:** es muy  similar al campo cliente, solo con escribir unas cuantas letras del nombre del vendedor o del numero de cedula te aparecera en la lista desplegable las sugerencias, tambien puedes buscar en la lupa con losb  filtros de busqueda avanzada.

**6) Fecha  de Nacimiento:** Si sabes la fecha de nacimiento del cliente la puedes seleccionar desde el icono de calendario, en caso de no disponer de la informacion la puedes dejar en blanco por que esta informacion no es obligatoria. 

**7) Nota:** Es opcional si deseas escribir una nota.

Total
-----

Productos
---------

**1) producto:**
Digita el nombre del producto

**2) Precio:**

**3) Impuesto:**

**4) Descripción:** este campo es opcional puedes o no escribir una descripcion.

**1) Boton :** :guilabel:`Guardar`.

.. Hint:: https://erpv3.source-solutions.co/facturacion/cliente/list.


