.. _inventario:

Inventario
==========

En este modulo puedes gestionar todo lo relacionado con inventario, como reportes, Movientos de inventario, existencias, bodegas, productos, variantes de productos entre muchas otras configuuracion de inventario.

				.. contents:: Tabla de Contenidos.


Configuración
+++++++++++++++

_________________________________________________________________________________________________
En este modulo vamos configurara los atributos de pantilla, categorias de prooducto, tipos de producto, tipos de unidad de medida, unididades de medida.

Atributos de  plantilla
++++++++++++++++++++++++

Los atributos son especificaciones de un objeto o producto,
en este caso de una forma tal vez más clara son las características de un producto.

Crear Atributo plantillas
-------------------------

**1: Boton:** :guilabel:`Nuevo`.

**2: Nombre:** (*Obligatorio) Ejemplo: Color.

**3: Descripcion:** Este espacio es opcion. 

**4: Valor:** Negro.

**5: Boton:** :guilabel:`+`.  En caso podriamos agregar mas colores rojo, azul, gris.

**8:** por ultimo click en el boton :guilabel:`Guardar`.

.. Hint:: https://erpv3.source-solutions.co/inventario/atributo-plantilla/list.


																																																																																																																																																																																																												
Categorias
++++++++++

Descripción:
------------
Las categorías se ejemplan para ordenar productos por criterios de clasificación 

Para agregar un nuevo registró
Esquina superior derecha encontramos el  Botón verde Nuevo.

Crear Categoria
---------------
**1:** Boton :guilabel:`Nuevo`.

**2: Categoria de producto:** Digita el nombre que va tener la categoría, o el su categoria padre, por el ejemplo el padre de lunes es semana y de semana es mes.

**3: Nombre:** simplemente digita el nombre que va tener la categoria este campo es (*Obligatorio) puedes crear una categoria sin categoria padre y esta seria la categoria padre entonces pero no puedes crear una categoria sin nombre.

**4: Acivo:** (casilla de verificacion o checkbox) para tener activa la categoriá.

**5: Visible en POS:** (casilla de verificacion o checkbox) al activar esta opcion perimitira que se muestre en el POS.

**6: Descripción:** Este espacio es opcion.

**7:** por ultimo click en el boton :guilabel:`Guardar`.

Busqueda de Categorias
----------------------

**1 Búsqueda de un registro**
*En la búsqueda tenemos tres filtros*  

**2: Categoría de producto**
*Filtrar los resultados de búsqueda por su categoría* 

**3: Nombre**
*Con este filtro podemos buscar por el nombre de los productos* 

**4: Activo**
*Esta casilla filtradora de productos solo tiene dos opciones (si) para activos y (no) para inactivos*

**5:** :guilabel:`Limpiar Filtros`.  
*Borra toda la información en los campos filtros de búsqueda*

**6:** :guilabel:`Buscar`.
*Este botón realizadas las buscadas con la información digitada en los campos filtros de búsqueda*



.. Hint:: https://erpv3.source-solutions.co/inventario/categoria-producto/list.


Unidades de Medida
++++++++++++++++++

Descripcion:
-------------
Una unidad de medida es una cantidad estandarizada de una determinada magnitud física, Cualquier valor de una cantidad física puede expresarse como un múltiplo de la unidad de medida.
La unidad principal para medir la masa es el gramo. Para medir masas mayores están los múltiplos (decagramo, hectogramo, kilogramo, tonelada… ) y para medir masas menores están los submúltiplos (decigramo, centigramo, miligramo…)

Crear Unidad de Medida
----------------------

**1):** Boton :guilabel:`Nuevo`.

**2) Nombre:** Digita el nombre va tener tu unidad de medida ejemplo kilo, centrimetro etc.  

**3) Descripción:** es espacio opcional.

**4:** Click el botón :guilabel:`Guardar`.


.. Hint:: https://erpv3.source-solutions.co/inventario/tipo-unidad/list.

Rutas de Despacho
+++++++++++++++++

Proximamente



Productos
+++++++++++

Descripcion
------------
Un producto es una opción elegible, viable y repetible que la oferta pone a disposición de la demanda, para satisfacer una necesidad o atender un deseo a través de su uso o consumo. 

Crear Producto
--------------

**1):** Boton :guilabel:`Nuevo`.

**2) Nombre:** Digita el nombre va tener tu unidad de medida ejemplo kilo, centrimetro etc.  

**3) Descripción:** es espacio opcional.

**4:** Click el botón :guilabel:`Guardar`.

Importar Productos
------------------

Descripcion
------------------

para la importación de productos de crear un archivo .CSV y subirlo en el orden los campos necesarios para su correcta carga si un si una columna que un nombre mal creado todo el proceso será invalidado por sistema.

Movimientos de Inventario
-------------------------

Descripcion
-------------

Los movimientos de inventario le ayudan a registrar todos los eventos relaciones 
Con las ingresos y salida de productos para poder identificar de forma fácil 
Cuál ha sido el movimiento de las existencias de sus artículos, el sistema B365 cuenta con Los conceptos de inventario tal como comprar, factura, remisión, traslado o traspaso, 
Traslado de sucursal, devolución.

**1):** Boton :guilabel:`Nuevo`.

**2) Unidad Organizaciónal:** Normalmente son la localizacion de una sede.

**3) Tipo Movimiento:** Este campo es obligatorio tiene tres tipo de movimiento translado, disminucion y incremento.

**4) Observación:** Este campo es opcional.

**5) Producto:** Este campo cuanta con el atributo de autocompletado basatara solo con escribas unas cuantas letras del nombre del producto y comenzaran aparacer las sugerencias de nombres ademas tambien tiene una lupa a la deraza del campo para realizar una busqueda avanzada.

**5) Unidad:** por lo general puedes encontrar las todas las bodegas de existencias simplemente escribiendo ex en lista desplegable te mostrara todas las bodega de existencias si quieres chequear todas las bodegas debes dar click la lupa.

**5) Boton:** :guilabel:`+ Adicionar Elemento`.


**6) Boton:** :guilabel:`Guardar`.


.. Hint:: https://erpv3.source-solutions.co/inventario/movimiento/list.

_______________________________________________________________________